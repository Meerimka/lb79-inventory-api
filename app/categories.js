const express = require('express');



const createRouter = connection =>{

    const router = express.Router();



    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `categories`', (error, results)=>{
            if(error){
                return  res.status(500).send({error:'Db not correct'})
            }
            return res.send(results);
        });
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `categories` WHERE `id`= ?',req.params.id, (error, results)=>{
            if(error){
                return res.status(500).send({error:'Db not correct'})
            }
            if(results[0]){
                return  res.send(results);
            }else{
                return  res.status(404).send({error:'Category not found'})
            }

        });
    });

    router.delete('/:id',(req, res)=>{
        connection.query('DELETE  FROM `categories` WHERE `id`=?',req.params.id, (error, results)=>{
            if(error){
                return  res.status(500).send({error:'Db not correct'})
            } else {
                return res.send({message: 'OK'})
            }
        })
    });

    router.post('/', (req, res) => {
        const category = req.body;
        connection.query('INSERT INTO `categories` (`title`,`description`) VALUES (?,?)',[category.title, category.description],
            (error,results)=>{
                return res.status(500).send({error: "DB not correct"})
            });
        return  res.send({message: 'OK'});
    });

    router.put('/:id',  (req, res) => {

        const category =req.body;
        console.log(category);

        category.id = req.params.id;

        let query = 'UPDATE `categories` SET `title`= ?, `description`= ?  WHERE `id`=?';

        const data = [category.title, category.description,category.id];

        connection.query(query, data,
            (error, results)=>{
                console.log(error);
                if(error){
                    return res.status(500).send({error:'Db not correct'})
                }
                return res.send(results);
            });

    });

    return router;
};



module.exports = createRouter;
