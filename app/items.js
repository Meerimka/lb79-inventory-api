const express = require('express');
const nanoid = require('nanoid');
const multer = require ('multer');
const path = require('path');
const config = require ('../config');


const createRouter = connection =>{

    const router = express.Router();


    const storage = multer.diskStorage({
        destination:  (req, file, cb)=> {
            cb(null, config.uploadPath);
        },
        filename:  (req, file, cb) => {
            cb(null, nanoid() + path.extname(file.originalname))
        }
    });


    const upload = multer({storage});

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `items`', (error, results)=>{
            if(error){
                return res.status(500).send({error:'Db not correct'})
            }
            return  res.send(results);
        });

    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `items` WHERE `id`= ?',req.params.id, (error, results)=>{
            if(error){
              return  res.status(500).send({error:'Db not correct'})
            }
            if(results[0]){
                return  res.send(results);
            }else{
                return  res.status(404).send({error:'Item not found'})
            }

        });
    });

    router.delete('/:id',(req, res)=>{
        connection.query('DELETE  FROM `items` WHERE `id`=?',req.params.id, (error, results)=>{
            if(error){
               return res.status(500).send({error:'Db not correct'})
            } else {
              return  res.send({message: 'OK'})
            }
        })
    });

    router.post('/', upload.single('image'), (req, res) => {
        const item =req.body;
        console.log(item);


        if(req.file){
            item.image = req.file.filename;
        }
        connection.query('INSERT INTO `items`(`category_id`,`place_id`,`title`, `description`, `image`) VALUES (?,?,?,?,?)',
            [item.category_id,item.place_id,item.title, item.description, item.image],
            (error, results)=>{
            console.log(error);
                if(error){
                    return res.status(500).send({error:'Db not correct'})
                }
                return res.send({message: 'OK'});
            });
        
    });

    router.put('/:id', upload.single('image'), (req, res) => {
        const item =req.body;
        item.id =req.params.id;

        let query = 'UPDATE `items` SET `title`= ?, `description`= ? WHERE `id`=?';

        const data = [item.title, item.description];


        if(req.file){
            item.image = req.file.filename;
            query = 'UPDATE `items`  SET `title`=?, `description`=?,`image`=? WHERE `id`=? ';

            data.push(item.image)
        }
        data.push(item.id);

        connection.query(query,data,
            (error, results)=>{
                console.log(error);
                if(error){
                    return res.status(500).send({error:'Db not correct'})
                }
                return res.send(results);
            });

    });

    return router;
};


module.exports = createRouter;
