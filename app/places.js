const express = require('express');


const createRouter = connection =>{
    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `places`', (error, results)=>{
            if(error){
                return res.status(500).send({error:'Db not correct'})
            }
            return res.send(results);
        });
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `places` WHERE `id`= ?',req.params.id, (error, results)=>{
            if(error){
                return  res.status(500).send({error:'Db not correct'})
            }
            if(results[0]){
                return res.send(results);
            }else{
                return  res.status(404).send({error:'Place not found'})
            }

        });
    });



    router.post('/', (req, res) => {
        const place = req.body;

        connection.query('INSERT INTO `places` (`title`,`description`) VALUES (?,?)',[place.title, place.description],
            (error,results)=>{
                return res.status(500).send({error: "DB not correct"})
            });
        return res.send({message: 'OK'});
    });

    router.put('/:id',  (req, res) => {

        const place =req.body;


        place.id = req.params.id;

        let query = 'UPDATE `categories` SET `title`= ?, `description`= ?  WHERE `id`=?';

        const data = [place.title, place.description,place.id];

        connection.query(query, data,
            (error, results)=>{
                console.log(error);
                if(error){
                    return res.status(500).send({error:'Db not correct'})
                }
                return res.send(results);
            });

    });


    return router;

};



module.exports = createRouter;
