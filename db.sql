CREATE SCHEMA `inventory` DEFAULT CHARACTER SET utf8 ;

USE `inventory` ;

CREATE TABLE `categories` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`title` VARCHAR(255) NOT NULL,
`description` TEXT
);

CREATE TABLE `places` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`));

  CREATE TABLE `items` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category_id` INT NULL,
  `place_id` INT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `image` VARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  INDEX `category_id_fk_idx` (`category_id` ASC),
  INDEX `place_id_fk_idx` (`place_id` ASC),
  CONSTRAINT `category_id_fk`
    FOREIGN KEY (`category_id`)
    REFERENCES `categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `place_id_fk`
    FOREIGN KEY (`place_id`)
    REFERENCES `places` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE);

    INSERT INTO `categories` (`id`,`title`,`description`)
    VALUES
    (1,'Office','some descriptions'),
    (2,'Drinks','some descriptions'),
    (3,'Users','some descriptions');

    INSERT INTO `places` (`id`,`title`,`description`)
    VALUES
    (1,'Office#1','some descriptions'),
    (2,'Office#2','some descriptions'),
    (3,'Office#3','some descriptions');

        INSERT INTO `items` (`id`,`category_id`,`place_id`,`title`,`description`)
    VALUES
    (1,2,1,'item1','some descriptions'),
    (2,1,2,'item2','some descriptions'),
    (3,3,3,'item3','some descriptions');






